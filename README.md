# Music player 🔥

![build](https://img.shields.io/badge/build-passing-brightgreen)
![version](https://img.shields.io/badge/version-1.2.0-brightgreen)
![type](https://img.shields.io/badge/type-frontend-brightgreen)

## Basic music player app for beginners

## Học được gì ?

- Nắm vững cách DOM
- Animate API
- Sử dụng video, audio, css cơ bản
- Sử dụng gg fonts, html5, icon, favicon

### By `Nguyễn Hoàng Nam`

### Thông tin khác

- Ngày thực hiện xong: `30/11/2021`
- Học theo [F8](https://fullstack.edu.vn/), `F8` là kênh học lập trình miễn phí nên các bạn cứ qua tìm hiểu, ủng hộ nha
- Xem video tại [Cách Code Music Player Xịn Xò Từ A-Z | Mồng một chăm chỉ để năm mới THÀNH CÔNG nào!!!](https://www.youtube.com/watch?v=vAecGPWxzFE)
